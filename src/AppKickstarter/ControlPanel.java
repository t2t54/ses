package AppKickstarter;

import javax.swing.*;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.List;

public class ControlPanel {
    public JPanel rootPanel;
    private JTable elevatorTable;
    private JScrollPane scrollPanel;
    private String[] tableHeader;
    private Object[][] tableData;
    private int selectedRowIndex;
    private String selectedMode;

    public ControlPanel(Object[][] tableData) {
        this.tableData = tableData;
    }

    public void setState(String eid, String state) {
        int r = 0;
        r = r * 26 + eid.charAt(0) - 'A' + 1;
        int c = 2;
        elevatorTable.getModel().setValueAt(state, r - 1, c);
        System.out.println("Control set state " + eid + state);
    }

    public void setCurrentFloor(String eid, int floor) {
        int r = 0;
        r = r * 26 + eid.charAt(0) - 'A' + 1;
        int c = 3;
        elevatorTable.getModel().setValueAt(Integer.toString(floor), r - 1, c);
        System.out.println("Control set floor " + eid + floor);

    }

    public void setDirection(String eid, String dir) {
        int r = 0;
        r = r * 26 + eid.charAt(0) - 'A' + 1;
        int c = 4;
        elevatorTable.getModel().setValueAt(dir, r - 1, c);
        System.out.println("Control set dir " + eid + dir);

    }

    public void setSchedule(String eid, List<Integer> eSchedule) {
        int r = 0;
        r = r * 26 + eid.charAt(0) - 'A' + 1;
        int c = 5;
        String route = "";
        if (eSchedule.get(0) > 0) {

            route = "[";
            for (int i = 0; i < eSchedule.size(); i++) {
                route += (" " + eSchedule.get(i) + "/F");
                if (i != eSchedule.size() - 1) {
                    route += ",";
                }
            }
            route += "]";
        }
        elevatorTable.getModel().setValueAt(route, r - 1, c);
        System.out.println("Control set eSchedule " + eid + route);

    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
        ControlTableModel model = new ControlTableModel(tableData);
        elevatorTable = new JTable(model);
        addComboBoxToStatusColumn(elevatorTable);

        elevatorTable.setFillsViewportHeight(true);
        elevatorTable.setPreferredScrollableViewportSize(elevatorTable.getPreferredSize());
        elevatorTable.setDragEnabled(false);
        elevatorTable.setRowSelectionAllowed(false);

        model.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
                //System.out.println( getEid(e.getFirstRow()) + " : " + elevatorTable.getModel().getValueAt( e.getFirstRow(),e.getColumn()));
            }
        });

        TableColumn column = null;
        for (int i = 0; i < model.getColumnCount(); i++) {
            column = elevatorTable.getColumnModel().getColumn(i);
            if (i == 5) {
                column.setPreferredWidth(150); //third column is bigger
            } else {
                column.setPreferredWidth(70);
            }
        }

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        for (int x = 0; x < model.getRowCount(); x++) {
            elevatorTable.getColumnModel().getColumn(x).setCellRenderer(centerRenderer);
        }

        elevatorTable.setRowHeight(50);
    }

    private String getEid(int row) {
        return (String) elevatorTable.getModel().getValueAt(row, 0);
    }

    private void addComboBoxToStatusColumn(JTable table) {
        TableColumn tmpColum = table.getColumnModel().getColumn(1);
        final JComboBox comboBox = new JComboBox();
        comboBox.addItem("ON");
        comboBox.addItem("OFF");
        DefaultCellEditor defaultCellEditor = new DefaultCellEditor(comboBox);
        tmpColum.setCellEditor(defaultCellEditor);
    }
}

