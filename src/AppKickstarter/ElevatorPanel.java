package AppKickstarter;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ElevatorPanel {
    public JPanel rootPanel;
    private JLabel doorState;
    private JLabel curentFloor;
    private JLabel direction;
    private JLabel schedule;
    private JLabel eName;

    public ElevatorPanel (String eid){
        eName.setText("Elevator " + eid);
    }

    public void setCurentFloor(int floorNum){
        curentFloor.setText(floorNum+"/F");
    }

    public void setSchedule(ArrayList<Integer> eSchedule){
        if (eSchedule.get(0) < 0 ){
            schedule.setText("");
            return;
        }
        String route = "[";
        for (int i = 0; i < eSchedule.size(); i++) {
            route += (" " + eSchedule.get(i) + "/F");
            if (i != eSchedule.size() - 1){
                route += ",";
            }
        }
        route += "]";
        schedule.setText(route);
    }

    public void changeDoorState(String state) {
        BufferedImage image;
        String imageName;

        if (state.equals("open")){
            imageName = "src/AppKickstarter/images/openedDoor.png";
        }else{
            imageName = "src/AppKickstarter/images/closedDoor.png";
        }

        try {
            image = ImageIO.read(new File(imageName));
            doorState.setIcon(new ImageIcon(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeDirState(Character state) {
        BufferedImage image;
        String imageName;
        if (state == 'U'){
            imageName = "src/AppKickstarter/images/arrow-up-6-64.png";
        }else if (state == 'D'){
            imageName = "src/AppKickstarter/images/arrow-down.png";
        }else{
            imageName = "src/AppKickstarter/images/dash.png";
        }

        try {
            image = ImageIO.read(new File(imageName));
            direction.setIcon(new ImageIcon(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
