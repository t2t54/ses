package AppKickstarter.myThreads;

import AppKickstarter.misc.*;
import AppKickstarter.AppKickstarter;
import AppKickstarter.timer.Timer;

import java.util.ArrayList;


/**
 * Elevator is used to define different properties itself
 */
public class Elevator {
    // Info of the elevator
    private String elevate_id;
    private Integer currentFloor;
    private Character direction;

    // State
    private Integer currentStates;
    private State[] states = {  new Opened(), new Closed(), new Moving(),  new Arrived(), new Idle(), new Off()};

    // Schedule
    private ArrayList<Integer> schedule;

    // Timer
    public double upOneFloorTime, downOneFloorTime, accUp, accDown, decUp, decDown, doorOpen, doorClose, doorWait;


    public Elevator (String id, double upOneFloorTime, double downOneFloorTime, double accUp, double accDown,
                     double decUp, double decDown, double doorOpen, double doorClose, double doorWait) {
        this.elevate_id = id;
        this.currentFloor = 0;
        this.currentStates = 4;
        this.direction = 'S';
        this.schedule = new ArrayList<Integer>();     //send id,state,current,dir,schedule
        this.schedule.add(Integer.MIN_VALUE);
        this.upOneFloorTime = upOneFloorTime;
        this.downOneFloorTime = downOneFloorTime;
        this.accUp = accUp;
        this.accDown = accDown;
        this.decUp = decUp;
        this.decDown = decDown;
        this.doorOpen = doorOpen;
        this.doorClose = doorClose;
        this.doorWait = doorWait;
    }

    public void openTheDoor()  { states[currentStates].openTheDoor(this);  };
    public void closeTheDoor() { states[currentStates].closeTheDoor(this); };
    public void departure() { states[currentStates].departure(this);  };
    public void moveTheElevator(int current) { states[currentStates].moveTheElevator(this, current);  };
    public void offElevator() { states[currentStates].offElevator(this);  };
    public void onElevator() { states[currentStates].onElevator(this);  };
    public void exitIdle() { states[currentStates].exitIdle(this);  };
    public void idle() { states[currentStates].idle(this);  };

    /**
     * Set current floor number
     * @param floor current floor number
     */
    public void setCurrentFloor(int floor) {
        this.currentFloor = floor;
    }

    /**
     * Add floor number to schedule
     * @param floor  floor number which is going to add to schedule
     */
    public void addSchedule(int floor) {
        this.schedule.add(floor);
    }

    /**
     * Get current floor number
     * @return current floor number
     */
    public int getCurrentFloor() {
        return this.currentFloor;
    }

    /**
     * Get current schedule
     * @return current schedule
     */
    public ArrayList<Integer> getSchedule() {
        return this.schedule;
    }

    /**
     * Get current state of specific elevator
     * @return elevator state
     */
    public String getState() {
        switch(this.currentStates) {
            case 0: return "open";
            case 1: return "close";
            case 2: return "move";
            case 3: return "arrive";
            case 4: return "idle";
            case 5: return "off";
            default: return "error";
        }
    }

    /**
     * Change current state of elevator
     * @param stateNo state case number
     */
    public void changeState(int stateNo) {
        this.currentStates = stateNo;
    }

    /**
     * Get current direction of elevator
     * @return current direction of elevator
     */
    public char getDirection() {
        return this.direction;
    }

    public void setSchedule(ArrayList<Integer> schedule) {
        this.schedule = schedule;
    }

    public String toString() {
        // System.out.println("currentStates: " + currentStates);
        return "Elevator " + this.elevate_id + " is " + this.states[currentStates].toString();
    } // toString

    /**
     * Abstract class of elevator state which can be overwrite
     */
    private abstract class State {
        void openTheDoor(Elevator elevator)  {
//            System.out.println("error");
        }
        void closeTheDoor(Elevator elevator) {
//            System.out.println("error");
        }
        void departure(Elevator elevator) {
//            System.out.println("error");
        }
        void moveTheElevator(Elevator elevator, int current)  {
//            System.out.println("error");
        }
        void offElevator(Elevator elevator) {
//            System.out.println("error");
        }
        void onElevator(Elevator elevator) {
//            System.out.println("error");
        }
        void exitIdle(Elevator elevator) {
//            System.out.println("error");
        }
        void idle(Elevator elevator) {
//            System.out.println("error");
        }
        public abstract String toString();
    }

    /**
     * Elevator door is opened
     */
    private class Opened extends State {

        void closeTheDoor(Elevator elevator) {

            try {
                Thread.sleep((int) doorClose * 1000);
            } catch (Exception e) {
            };

            elevator.currentStates = 1;

        }

        void offElevator(Elevator elevator) {

            elevator.closeTheDoor();
            elevator.idle();
            elevator.currentStates = 5;

        }

        public String toString() { return "Opened"; }
    }

    /**
     * Elevator door is closed
     */
    private class Closed extends State {

        void openTheDoor(Elevator elevator) {

            if (elevator.currentFloor == elevator.schedule.get(0)) {
                elevator.schedule.remove(0);
            }

            elevator.currentStates = 0;

        }

        void departure(Elevator elevator) {

            if ( elevator.schedule.get(0) < 0 ) {
                elevator.idle();
                return;
            }

            try {
                if (elevator.currentFloor > elevator.schedule.get(0)) {
                    elevator.direction = 'D';

                } else if (elevator.currentFloor < elevator.schedule.get(0)) {
                    elevator.direction = 'U';
                }

                elevator.currentStates = 2;
            } catch (Exception e) { System.out.println(e); }

        }

        void idle(Elevator elevator) {

            elevator.direction = 'S';
            elevator.currentStates = 4;

        }

        void offElevator(Elevator elevator) {

            elevator.idle();
            elevator.currentStates = 5;

        }

        public String toString() { return "Closed"; } // toString
    }


    /**
     * Elevator is moving
     */
    private class Moving extends State {

        // Moving
        void moveTheElevator(Elevator elevator, int current) {
            int destinationFloor;
            double travelTime;
            int originFloor = current;

            destinationFloor = elevator.schedule.get(0);

            travelTime = countTravelTime(elevator.currentFloor, destinationFloor, originFloor) * 1000;

            try {
                Thread.sleep((int) travelTime);
            } catch (Exception e) {};

            if (elevator.currentFloor > destinationFloor) {
                elevator.currentFloor--;
            } else {
                elevator.currentFloor++;
            }

            System.out.println("elevator.currentFloor: " + elevator.currentFloor);
            System.out.println("destinationFloor: "+destinationFloor);
            if (elevator.currentFloor == destinationFloor) {
                System.out.println("arrived");
                elevator.schedule.remove(0);
                if (schedule.size() == 0) {
                    elevator.direction = 'S';
                    elevator.schedule.add(Integer.MIN_VALUE);
                } else {
                    if (elevator.currentFloor > elevator.getSchedule().get(0)) {
                        elevator.direction = 'D';
                    } else {
                        elevator.direction = 'U';
                    }
                }
                elevator.currentStates = 3;
            }
        }


        public double countTravelTime(int currentF, int destinationF, int originF) {



            if (originF < destinationF) {

                if (currentF == originF) {

                    return accUp;

                } else if (currentF == destinationF - 1) {

                    return decUp;

                } else {

                    return upOneFloorTime;

                }

            } else {

                if (currentF == originF) {

                    return accDown;

                } else if (currentF == destinationF + 1) {

                    return decDown;

                } else {

                    return downOneFloorTime;

                }

            }
        }

        void offElevator(Elevator elevator) {
            elevator.currentStates = 3;
            elevator.openTheDoor();
            elevator.closeTheDoor();
            elevator.idle();
            elevator.currentStates = 5;
        }

        public String toString() { return "Moving"; } // toString
    }

    /**
     * Elevator is arrived
     */
    private class Arrived extends State {

        void openTheDoor(Elevator elevator) {
//            System.out.print(elevate_id + ": elevator.currentFloor" + elevator.currentFloor);
//            System.out.print(elevate_id + ": elevator.schedule.get(0)" + elevator.schedule.get(0));
//            if (elevator.currentFloor == elevator.schedule.get(0)) {
//                elevator.schedule.remove(0);
//            }

            try {
                Thread.sleep((int) doorOpen * 1000);
            } catch (Exception e) {};

            elevator.currentStates = 0;
        }

        void offElevator(Elevator elevator) {

            elevator.openTheDoor();

            elevator.closeTheDoor();

            elevator.idle();

            elevator.offElevator();
        }

        public String toString() { return "Arrived"; } // toString
    }

    /**
     * Elevator is off
     */
    private class Off extends State {

        void onElevator(Elevator elevator) {

            elevator.currentStates = 4;

        }

        public String toString() { return "Off"; } // toString
    }

    /**
     * Elevator is idle
     */
    private class Idle extends State {

        void exitIdle(Elevator elevator) {

            elevator.currentStates = 1;

        }

        void offElevator(Elevator elevator) {

            elevator.currentStates = 5;
        }

        public String toString() { return "Idle"; } // toString
    }


}

