package AppKickstarter.myThreads;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.Msg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSender extends AppThread {
    private final PrintWriter out;
    private String outNetMsg;

    public ServerSender(String id, AppKickstarter appKickstarter, PrintWriter out) {
        super(id, appKickstarter);
        this.out = out;
        outNetMsg = null;
    }

    public void run() {
        log.info(id + ": starting...");
            for (boolean quit = false; !quit;) {

                //receiveMsgFromECT
                Msg msg = mbox.receive();

                //get liftNo
                switch (msg.getType()) {
                    case Svc_Reply:
                        outNetMsg = "Svc_Reply "+ msg.getDetails();
                        out.println(outNetMsg);
                        //outNetMsg = "Svc_Reply "+ request[1] + " "+ request[2] + " "+ request[3] +" "+msg.getDetails();
                        break;

                    case Elev_Arr:
                        log.info("=============ARRIVAL================ ");
                        outNetMsg = "Elev_Arr " + msg.getDetails();
                        out.println(outNetMsg);
                        log.info(id + ": Arrival msg: " + outNetMsg);
                        log.info("======================================= ");
                        break;

                    case Elev_Dep:
                        log.info("=============DEPARTURE================ ");
                        outNetMsg = "Elev_Dep " + msg.getDetails();
                        out.println(outNetMsg);
                        log.info(id + ": Depart msg: " + outNetMsg);
                        log.info("======================================= ");
                        break;

                    default:
                        log.severe(id + ": unknown message type!!");
                        log.info("Type = " + msg.getType());
                        break;
                }

            }

    }
}
