package AppKickstarter.myThreads;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;
import AppKickstarter.timer.Timer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *  A class extended the AppThread
 *  ElevatorMsgReceiver is used to receive elevator allocation from EControlThread
 *  New schedule is formed with new elevator allocation
 */
public class ElevatorMsgReceiver extends AppThread {
    private String elevatorId = "";
    private final int sleepTime = 5;

    //------------------------------------------------------------
    // ThreadA
    public ElevatorMsgReceiver(String id, AppKickstarter appKickstarter, String elevatorId) {
        super(id, appKickstarter);
        this.elevatorId = elevatorId;
    } // ThreadA


    //------------------------------------------------------------
    // run
    public void run() {
        log.info(id + ": starting...");
//        Timer.setSimulationTimer(id, mbox,sleepTime);
        int mCnt = 0;

        for (boolean quit = false; !quit;) {
            Msg msg = mbox.receive();

            log.info(id + ": message received: [" + msg + "].");

            AppThread elevatorThd = appKickstarter.getThread(elevatorId);
            ElevatorThread elevatorThread = (ElevatorThread) elevatorThd;

            switch (msg.getType()) {
                case Allocation:

//                    log.info(id + ": receiving timesup at " + appKickstarter.getSimulationTimeStr());
                    log.info(id + ": message received: [" + msg + "].");

                    String[] splitedMsg = msg.getDetails().split(",");
                    ArrayList<Integer> newSchedule = new ArrayList<Integer>();

                    for (String i: splitedMsg) {
                        newSchedule.add(Integer.parseInt(i));
                    }

                    elevatorThread.setSchedule(newSchedule);

//                    log.info(id + ": receiving timesup at " + appKickstarter.getSimulationTimeStr());
//                    log.info(id + ": say hello to Thread B...");

//                    AppThread elevatorThd = appKickstarter.getThread("ElevatorThread" );
//                    if (elevatorThd instanceof ElevatorThread){
//                        System.out.println("ele");
//                        ElevatorThread eT = (ElevatorThread) elevatorThd;
//                        eT.setSchedule();
//                    }

                    // time to say hello to Thread B
//                    MBox thdBMBox = thdB.getMBox();
//                    AppThread thdB = appKickstarter.getThread("EControlThread");
//                    thdBMBox.send(new Msg(id, mbox, Msg.Type.Hello, "Hello, this is Thread A!  (mCnt: " + ++mCnt + ")"));

//                    sleep again
//                    Timer.setSimulationTimer(id, mbox, sleepTime);
                    break;

//                case HiHi:
//                    log.info(id + ": " + msg.getSender() + " is saying HiHi to me!!!");
//                    break;
                case TurnedOff:
//                    elevatorThread.turnOff();
//
                case Terminate:
                    quit = true;
                    break;

                default:
                    log.severe(id + ": unknown message type!!");
                    break;
            }
        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run
}
