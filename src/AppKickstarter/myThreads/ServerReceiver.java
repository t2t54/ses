package AppKickstarter.myThreads;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;

import java.io.BufferedReader;
import java.io.IOException;

public class ServerReceiver extends AppThread {

    private final BufferedReader in;
    private String inNetMsg;

    public ServerReceiver(String id, AppKickstarter appKickstarter, BufferedReader in) {
        super(id, appKickstarter);
        this.in = in;
    }

    public void run() {
        log.info(id + ": starting...");

        try {

            inNetMsg = null;

            while ((inNetMsg = in.readLine()) != null) {

                String[] request = inNetMsg.split(" ");
                sendReqToECT(inNetMsg);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is designed to send Svc_Req to EControlThread
     * @param inNetMsg
     */
    public void sendReqToECT(String inNetMsg){
        // time to say hello to Thread B
        AppThread thdB = appKickstarter.getThread("EControlThread");
        MBox thdBMBox = thdB.getMBox();
        thdBMBox.send(new Msg(id, mbox, Msg.Type.Svc_Req, inNetMsg));


    }
}
