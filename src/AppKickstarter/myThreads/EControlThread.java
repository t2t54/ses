
package AppKickstarter.myThreads;

import java.util.*;
import java.util.stream.Collectors;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;

import AppKickstarter.ControlPanel;

import javax.swing.*;


    /**
     *  A class extended the AppThread
     *  EControlThread is used to communicate between ServerThread and ElevatorThread
     *  EControlThread is designed for receiving and sending message
     */
public class EControlThread extends AppThread {
    private final int sleepTime = 5;
    private final int NElevators = Integer.parseInt(appKickstarter.getProperty("Bldg.NElevators"));
    private ElevatorStatus[] elevatorStatus;


    //------------------------------------------------------------
    // EControlThread
    public EControlThread(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        elevatorStatus = new ElevatorStatus[NElevators];
    } // EControlThread

    /**
     *  The run method is started with timer function when the EControlThread is running
     *  The run method will be stopped when terminated
     */
    public void run() {
        log.info(id + ": starting...");
        //Timer.setSimulationTimer(id, mbox,sleepTime);
//        int mCnt = 0;
        List<Object> tempList = new ArrayList<Object>();

        for (int i = 0; i < NElevators; i++) {
            char tempId = (char) ((i + 1) + '0' + 16);
            elevatorStatus[i] = new ElevatorStatus(String.valueOf(tempId), "ON");
            Object tempObj = new String[]{elevatorStatus[i].getID(), "ON", "idle", "0", "S", ""};
            tempList.add(tempObj);
        }

        Object[][] tempArr = tempList.toArray(new Object[tempList.size()][]);
        ControlPanel controlPanel = new ControlPanel(tempArr);
        JFrame frame = new JFrame("ControlPanel");
        frame.setContentPane(controlPanel.rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 550);
        frame.setVisible(true);
//        frame.setResizable(false);

        for (boolean quit = false; !quit; ) {
            Msg msg = mbox.receive();

            log.info(id + ": message received: [" + msg + "].");

            switch (msg.getType()) {
                case TimesUp:
                    //log.info(id + ": receiving timesup at " + appKickstarter.getSimulationTimeStr());

//                    // time to say hello to Thread B
//                    AppThread thdB = appKickstarter.getThread("ThreadB");
//                    MBox thdBMBox = thdB.getMBox();
//                    thdBMBox.send(new Msg(id, mbox, Msg.Type.Allocation, "Hello, this is elevator control thread!  (mCnt: " + ++mCnt + ")"));

                    // sleep again
                    // Timer.setSimulationTimer(id, mbox, sleepTime);
                    break;

                case ShowStatus:
                    log.info(msg.getSender() + ": "  + msg.getDetails());
                    String[] statusMsg = msg.getDetails().split("\\[")[0].split(" ");
                    String[] scheduleMsg = msg.getDetails().split("\\[")[1].replaceAll("\\]","").split(", ");

                    int Lid = msg.getSender().charAt(0) - 'A' ;
                    elevatorStatus[Lid].setId(statusMsg[0]);
                    elevatorStatus[Lid].setState(statusMsg[1]);
                    elevatorStatus[Lid].setCurrentFNo(Integer.parseInt(statusMsg[2]));
                    elevatorStatus[Lid].setDir(statusMsg[3]);

                    controlPanel.setState(elevatorStatus[Lid].getID(), elevatorStatus[Lid].getState());
                     controlPanel.setCurrentFloor(elevatorStatus[Lid].getID(), elevatorStatus[Lid].getCurrentFNo());
                    controlPanel.setDirection(elevatorStatus[Lid].getID(), elevatorStatus[Lid].getDir());

                    List<Integer> scheduleList = new ArrayList<>();
                    for (String fno : scheduleMsg){
                        if (!fno.equalsIgnoreCase(""))//{
                            scheduleList.add(Integer.parseInt(fno));
                    }


                    elevatorStatus[Lid].setSchedule(scheduleList);
                    controlPanel.setSchedule(elevatorStatus[Lid].getID(),elevatorStatus[Lid].getSchedule());
                    String scheduleString = scheduleList.stream().map(Object::toString).collect(Collectors.joining(" "));
                    String msgDetails = String.join(" ", elevatorStatus[Lid].getID(), Integer.toString(elevatorStatus[Lid].getCurrentFNo()), elevatorStatus[Lid].getDir(), scheduleString );
                    if (elevatorStatus[Lid].getState().equalsIgnoreCase("arrive")){

                        //Return to ST
                        AppThread st = appKickstarter.getThread("ServerSender");
                        MBox stMBox = st.getMBox();
                        stMBox.send(new Msg(id, mbox, Msg.Type.Elev_Arr, msgDetails));


                        System.out.println("SENT > "+ msgDetails);
                    }
                    break;

                case ShowDep:
                    log.info(msg.getSender() + ": "  + msg.getDetails());
                    String[] depStatusMsg = msg.getDetails().split("\\[")[0].split(" ");
                    String[] depScheduleMsg = msg.getDetails().split("\\[")[1].replaceAll("\\]","").split(", ");

                    String schedule = String.join(" ", depScheduleMsg);

                    String depMsgDetails = String.join(" ", depStatusMsg[0], depStatusMsg[2], depStatusMsg[3], schedule);
                    //Return to ST
                    AppThread st = appKickstarter.getThread("ServerSender");
                    MBox stMBox = st.getMBox();
                    stMBox.send(new Msg(id, mbox, Msg.Type.Elev_Dep, depMsgDetails));
                    break;

                case Svc_Req:
                    log.info(id + ": " + msg.getSender() + " sending request : ");
                    //Allocation
                    allocateElevator(msg.getDetails());
                    break;

                case Terminate:
                    quit = true;
                    break;

                default:
                    log.severe(id + ": unknown message type!!");
                    break;
            }
        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run


    /**
     *  AllocateElevator method is used to allocate schedule to elevator
     *  Reply will be sent to ServerThread while allocation will be sent to ElevatorThread
     *  @param PSRequestString   -- The elevator request from ServerThread
     */
    private void allocateElevator(String PSRequestString){
        String[] request = PSRequestString.split(" ");
        int srcFNo = Integer.parseInt(request[2]);
        int dstFNo = Integer.parseInt(request[3]);
        ArrayList<Integer> available = new ArrayList<Integer>();
        String allocatedElevator = "";

        double averageScheduleNum = 0;

        // Check available elevator
        for (int i = 0; i < NElevators; i++) {

            List<Integer> elevatorSchedule = elevatorStatus[i].getSchedule();

            //calculate average schedule ammount
            if (elevatorStatus[i].getSchedule().get(0) >= 0) {
                averageScheduleNum += elevatorStatus[i].getSchedule().size();
            }

            try {
                switch (elevatorStatus[i].getDir()) {
                    // if the elevator is free
                    case "S":
                        available.add(i);

                        break;

                    // if same direction && isPathTo the passenger, add to list
                    case "U":
                        if (elevatorSchedule.get(elevatorSchedule.size() - 1) - elevatorSchedule.get(0) >= 0) {
                            if ((srcFNo >= elevatorStatus[i].getCurrentFNo() && srcFNo <= dstFNo) ) {
                                available.add(i);
                            }
                        } else {
                            if (srcFNo - dstFNo > 0) {
                                available.add(i);
                            }
                        }

                        break;
                    case "D":
                        if (elevatorSchedule.get(elevatorSchedule.size() - 1) - elevatorSchedule.get(0) <= 0) {
                            if (srcFNo <= elevatorStatus[i].getCurrentFNo() && srcFNo >= dstFNo ) {
                                available.add(i);
                            }
                        } else {
                            if (srcFNo - dstFNo < 0) {
                                available.add(i);
                            }
                        }


                        break;
                    default:
                }
            } catch (Exception e) { System.out.println(e); }
        }

        averageScheduleNum = averageScheduleNum/NElevators;
        System.out.println("averageScheduleNum: " + averageScheduleNum);

        // Check which elevator is most suitable in the available list
        // less schedule -> less distance
//        for (int i = 0; i < elevatorStatus.length; i++) {
//            for (int y = 0; y < elevatorStatus.length; y++) {
//
//            }
//        }

        // allocate a most closely elevator to passenger
        if (available.size() > 0) {
            int nearest = available.get(0);
            int elevatorIdx, distance;
            double nearestDistance = (int) Math.sqrt(Math.pow(srcFNo - elevatorStatus[nearest].getCurrentFNo(), 2));

            for (int i = 0; i < available.size(); i++) {
                // elevatorId = elevatorStatus[available.get(i)].getID();
                elevatorIdx = available.get(i);
                System.out.println("elevatorIdx: " + elevatorIdx);
                distance = (int) Math.sqrt(Math.pow(srcFNo - elevatorStatus[elevatorIdx].getCurrentFNo(), 2));
                System.out.println("distance: " + distance + " nearestDistance: " + nearestDistance);
                System.out.println("elevatorStatus[elevatorIdx].getSchedule().size(): " + elevatorStatus[elevatorIdx].getSchedule().size() + " averageScheduleNum: " + averageScheduleNum);
                if (elevatorStatus[elevatorIdx].getSchedule().get(0) < 0 || elevatorStatus[elevatorIdx].getSchedule().size() <= averageScheduleNum) {
                    System.out.println("is less schedule");
                    if (distance <= nearestDistance) {
                        nearestDistance = distance;
                        nearest = elevatorIdx;
                    }
                }
                System.out.println("nearest: " + nearest);
            }
            allocatedElevator = elevatorStatus[nearest].getID();

            List<Integer> updatedSchedule = elevatorStatus[nearest].getSchedule();

            String originDirect;
            if (updatedSchedule.get(updatedSchedule.size() - 1) - updatedSchedule.get(0) > 0) {
                originDirect = "U";
            } else if (updatedSchedule.get(updatedSchedule.size() - 1) - updatedSchedule.get(0) < 0) {
                originDirect = "D";
            } else {
                originDirect = elevatorStatus[nearest].getDir();
            }

            System.out.println("originDirect: " + originDirect);

            // Clear the initial floor: MIN_VALUE
            if ((updatedSchedule.size() == 1) && updatedSchedule.get(0) == Integer.MIN_VALUE) {

                updatedSchedule.clear();
            }

            if (!updatedSchedule.contains(srcFNo) && elevatorStatus[nearest].getCurrentFNo() != srcFNo) {
                updatedSchedule.add(srcFNo);
            }

            if (!updatedSchedule.contains(dstFNo)) {
                updatedSchedule.add(dstFNo);
            }

            switch (originDirect) {
                case "U":
                    Collections.sort(updatedSchedule);
                    break;
                case "D":
                    Collections.sort(updatedSchedule);
                    Collections.reverse(updatedSchedule);
                    System.out.println("Down: " + updatedSchedule);
                    break;
                default:
                    System.out.println("first schedule");
            }

            System.out.println("updatedSchedule: " + updatedSchedule);

            //remove duplicates, sort
            //HashSet hs = new HashSet();
            //hs.addAll(updatedSchedule);
            //updatedSchedule.clear();
            //updatedSchedule.addAll(hs);
            //Collections.sort(updatedSchedule);

            String scheduleString = updatedSchedule.stream().map(Object::toString).collect(Collectors.joining(","));

            log.info("Allocation From Controller:" + scheduleString);
            //get

            //Return to ST
            AppThread st = appKickstarter.getThread("ServerSender");
            MBox stMBox = st.getMBox();
            stMBox.send(new Msg(id, mbox, Msg.Type.Svc_Reply, request[1] + " " + request[2] + " " + request[3] + " " + allocatedElevator));
            //Return to ET
            AppThread et = appKickstarter.getThread("elevatorMsgReceiver" + allocatedElevator);
            MBox etMBox = et.getMBox();
            etMBox.send(new Msg(id, mbox, Msg.Type.Allocation, scheduleString));
        }

    }

}
