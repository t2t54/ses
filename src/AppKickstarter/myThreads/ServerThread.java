package AppKickstarter.myThreads;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.AppThread;
import AppKickstarter.misc.MBox;
import AppKickstarter.misc.Msg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

/**
 *  A class extended the AppThread
 *  ServerThread is used to communicate with EControlThread and PassengerStream
 *  ServerThread is designed for receiving message from PassengerStream and EControlThread
 *  ServerThread is designed for sending message to EControlThread
 */
public class ServerThread extends AppThread {
    private final int sleepTime = 1;
    private final String svrIP = "127.0.0.1";
    private final int svrPort = 54321;

    private EControlThread eControlThread;
    private ServerReceiver serverReceiver;
    private ServerSender serverSender;
    //public String outNetMsg = null;


    public ServerThread(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        eControlThread = new EControlThread("EControlThread", appKickstarter);
    }

    /**
     *  The run method is started with timer function when the ServerThread is running
     *  The run method will be stopped when terminated
     */
    public void run() {
        log.info(id + ": Listening...");
        new Thread(eControlThread).start();


        try {

            ServerSocket Server = new ServerSocket(svrPort);
            Socket clientSocket = Server.accept();
            System.out.println(" THE CLIENT" + " " + clientSocket.getInetAddress()
                    + ":" + clientSocket.getPort() + " IS CONNECTED ");
            // Receiving Msg
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            serverReceiver = new ServerReceiver("ServerReceiver", appKickstarter, in);
            serverSender = new ServerSender("ServerSender", appKickstarter, out);
            new Thread(serverReceiver).start();
            new Thread(serverSender).start();

        } catch (IOException e) {
            e.printStackTrace();
        }


        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    } // run

}

