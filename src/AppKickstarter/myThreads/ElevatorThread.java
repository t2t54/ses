package AppKickstarter.myThreads;

import AppKickstarter.AppKickstarter;
import AppKickstarter.misc.*;
import AppKickstarter.timer.Timer;
import AppKickstarter.ElevatorPanel;

import javax.swing.*;
import java.util.ArrayList;

/**
 *  A class extended the AppThread
 *  ElevatorThread is used to communicate with EControlThread
 *  ElevatorThread is designed for set schedule for elevators
 *  The elevator GUI is displayed when ElevatorThread is running
 */

public class ElevatorThread extends AppThread {
    private final int sleepTime = 5;
    private Elevator elevator;
    private ElevatorMsgReceiver msgReceiver;
    private boolean elevatorIsOn = true;
    private double upOneFloorTime = Double.parseDouble(appKickstarter.getProperty("Elev.Time.UpOneFloor"));
    private double downOneFloorTime = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DownOneFloor")));
    private double accUp = Double.parseDouble((appKickstarter.getProperty("Elev.Time.AccUp")));
    private double accDown = Double.parseDouble((appKickstarter.getProperty("Elev.Time.AccDown")));
    private double decUp = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DecUp")));
    private double decDown = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DecDown")));
    private double doorOpen = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DoorOpen")));
    private double doorClose = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DoorClose")));
    private double doorWait = Double.parseDouble((appKickstarter.getProperty("Elev.Time.DoorWait")));

    //------------------------------------------------------------
    // ThreadA
    public ElevatorThread(String id, AppKickstarter appKickstarter) {
        super(id, appKickstarter);
        elevator = new Elevator(id, upOneFloorTime, downOneFloorTime, accUp, accDown, decUp, decDown, doorOpen, doorClose, doorWait);
        msgReceiver = new ElevatorMsgReceiver("elevatorMsgReceiver" + id, appKickstarter, id);
    } // ThreadA

    /**
     * New schedule is set up for the specific elevator
     * @param newSchedule   -- ArrayList of schedule
     */
    public void setSchedule(ArrayList<Integer> newSchedule) {
        System.out.println("RECEIVED ALLOCATE");
        elevator.setSchedule(newSchedule);
        System.out.println("Finished: " + elevator.getSchedule() + "," + elevator.getState());

    }

//    public void turnOff() {
//        elevator.offElevator();
//    }

    /**
     *  The run method is started with timer function when the ElevatorThread is running
     *  The run method will be stopped when terminated
     *  Elevator GUI is called and displayed
     *  Elevator action like open door, close door and move is called depending on schedule
     */
    public void run() {
        new Thread(msgReceiver).start();

        log.info(id + ": starting...");

        Timer.setSimulationTimer(id, mbox,sleepTime); // find run time
        int mCnt = 0;
        AppThread EControlThread = appKickstarter.getThread("EControlThread");
        MBox EControlThMBox = EControlThread.getMBox();
        EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " + elevator.getDirection() + " " + elevator.getSchedule()));


        JFrame frame = new JFrame("Elevator Panel");
        System.out.println(id);
        ElevatorPanel ePanel = new ElevatorPanel(id);
        frame.setContentPane(ePanel.rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 570);
        frame.setVisible(true);
        frame.setResizable(false);
        setGUI(ePanel);

        for (boolean quit = false; !quit;) {

            //log.info("Elevator" + id + ": current floor: " + elevator.getCurrentFloor() + ", state: " + elevator.getState() + ", direction: " + elevator.getDirection() + ", schedule: " + elevator.getSchedule());
            // log.info(elevator.toString());

            // System.out.println(this.id + ": elevator.getSchedule().get(0): " + elevator.getSchedule().get(0));

            if (elevator.getSchedule().get(0) >= 0) {
                System.out.print("not idle");

                elevator.exitIdle();
                // log.info(elevator.toString() + ":" + elevator.getSchedule());

                if (elevator.getCurrentFloor() != elevator.getSchedule().get(0)) {
                    EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowDep,  id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " + elevator.getDirection() + " " + elevator.getSchedule()));

                    elevator.departure();
                    // log.info(elevator.toString() + ":" + elevator.getSchedule());

                    EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " + elevator.getDirection() + " " + elevator.getSchedule()));

                    // log.info(elevator.toString());
                    if (elevator.getSchedule().get(0) >= 0) {
                        int tmpCurrent = elevator.getCurrentFloor();
                        int destination = elevator.getSchedule().get(0);
                        while (elevator.getCurrentFloor() != destination) {
                            elevator.moveTheElevator(tmpCurrent);
                            // log.info(elevator.toString() + ":" + elevator.getSchedule());
                            EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() +  " " + elevator.getCurrentFloor() + " "  + elevator.getDirection() + " " + elevator.getSchedule()));
                            ePanel.setCurentFloor(elevator.getCurrentFloor());
                            ePanel.changeDirState(elevator.getDirection());
                        }
                    }
                }

                // log.info(elevator.toString() + ":" + elevator.getSchedule());
                // !! send arrive message to server !!
                EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " +  elevator.getDirection() + " " + elevator.getSchedule()));

                // log.info(elevator.toString() + ":" + elevator.getSchedule());

                elevator.openTheDoor();
                EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " +  elevator.getDirection() + " " + elevator.getSchedule()));
                // log.info(elevator.toString() + ":" + elevator.getSchedule());
                setGUI(ePanel);
                // !! send opened door message to server ??

                // Load passenger ??
                try {
                    Thread.sleep((int) doorWait * 1000);
                } catch (Exception e) {};

                // !! send closing door message to server ??
                elevator.closeTheDoor();
                EControlThMBox.send(new Msg(id, mbox, Msg.Type.ShowStatus, id + " " + elevator.getState() + " " + elevator.getCurrentFloor() + " " +  elevator.getDirection() + " " + elevator.getSchedule()));
                // log.info(elevator.toString());
                setGUI(ePanel);

            } else {
                //log.info(elevator.toString() + "=" + elevator.getSchedule());
                //int temp = elevator.getSchedule().get(0);

                elevator.idle();
                setGUI(ePanel);
            }


//           ePanel.changeDoorState(elevator.getState());
//            ePanel.changeDirState(elevator.getDirection());
//            ePanel.setCurentFloor(elevator.getCurrentFloor());
//            ePanel.setSchedule(new int[]{1,2,5});


        }

        // declaring our departure
        appKickstarter.unregThread(this);
        log.info(id + ": terminating...");
    }// run

    private void setGUI(ElevatorPanel ePanel){
        ePanel.changeDoorState(elevator.getState());
        ePanel.changeDirState(elevator.getDirection());
        ePanel.setCurentFloor(elevator.getCurrentFloor());
        ePanel.setSchedule(elevator.getSchedule());
    }
}

