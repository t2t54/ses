package AppKickstarter.myThreads;

import java.util.List;

/**
 * ElevatorStatus define different status of elevator
 * These status data will be used for communication between ElevatorThread and EControlThread
 */
public class ElevatorStatus {
    private String id;
    private String mode = "ON";

    private String state  = "idle";
    private int currentFNo  = 0;
    private String dir  = "S";
    private List<Integer> schedule;

    public ElevatorStatus(){

    }

    public ElevatorStatus(String id, String mode){
        this.id = id;
        this.mode = mode;
    }

    public String getMode(){return this.mode;}
    public String getID() {
        return this.id;
    }

    public String getState() {
        return this.state;
    }

    public int getCurrentFNo() {
        return this.currentFNo;
    }

    public String getDir() {
        return this.dir;
    }

    public List<Integer> getSchedule() {
        return this.schedule;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setState(String state){
        this.state = state;
    }

    public void setCurrentFNo(int currentFNo){
        this.currentFNo = currentFNo;

    }

    public void setDir(String dir){
        this.dir = dir;
    }

    public void setSchedule(List<Integer> schedule){
        this.schedule = schedule;

    }

}
