package AppKickstarter;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

public class ControlTableModel extends AbstractTableModel {

    String[] columnNames = {"Elevator", "On/Off", "State", "Current floor", "Direction", "Schedule"};

    Object[][] data = {};

    public ControlTableModel(Object[][] data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) { return columnNames[col]; }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }

    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }

    public boolean isCellEditable(int row, int col) {
        if(col == 1) return true ;
        return false;
    }



}
